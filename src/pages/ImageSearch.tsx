export default function ImageSearch() {
    return (
        <>
            <article className="container">
                <header>
                    <h2>
                        Image Search
                    </h2>
                </header>
                <section>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce imperdiet, mi ut sodales efficitur, ante eros pretium massa, sit amet facilisis massa erat quis arcu. Donec aliquam lectus vestibulum tempus egestas. Morbi ut luctus sapien. Curabitur fringilla eu nisi eget tempus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ultricies nisi fringilla mi maximus viverra. Donec et pharetra dolor. Nullam placerat lacus erat. Morbi non aliquam risus. Sed sit amet lacinia risus. In sed viverra odio. Quisque pretium sapien sed ante sollicitudin pharetra. Nullam vel consequat odio. Donec mi dui, placerat id massa in, blandit feugiat quam. Integer non cursus dui.
                    </p>
                    <p>
                        TODO: Content
                    </p>
                </section>
                {/* <footer></footer> */}
            </article>
        </>
    );
}
