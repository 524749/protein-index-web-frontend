import { FormEvent, useRef, useState } from "react";
import TextInput from 'react-autocomplete-input';
import { Col, Form, InputGroup, Row, Spinner } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import ProteinTable from "../components/ProteinTable";
import NglPreview from "../components/NglPreview";

import "../css/protein-search.css";
import 'react-autocomplete-input/dist/bundle.css';
import LoadInfoBox from "../components/LoadInfoBox";

type ApiResult = {
    bucket: number[];
    bucket_order: number;
    bucket_prob_sequence: number[];
    distance: number;
    found_bucket_sequence: number[][];
    found_sequence: number[][];
    n_objects_in_bucket: number;
    node_order: number;
    object_id: string;
    prob_sequence: number[];
};

type ApiResults = {
    linear_search_time: number;
    results: ApiResult[];
    search_time: number;
};

enum LoadingState {
    Initial,
    Loading,
    ShowingResults,
};

type SearchResult = {
    rows: PreparedRow[];
    meta: {
        searchTime: number | null;
        linearSearchTime: number | null;
        totalSearchTime: number | null;
    }
};

export type onSearchHeader = (event: FormEvent, value: string | undefined) => void;
export type onLoadMoreHeader = (event: FormEvent) => void;

export type PreparedRow = {
    name: string;
    distance: number;
    organism: null | string;
    uniProtId: null | string
};

const inputHints = [
    "2VL3:B","5LF6:E","4I4N:C","6GWK:O","6YW7:D","3V3H:B","6ZZB:A","3H71:B","2APS:B","6KPQ:A","5KYE:D","1FBX:I","7EGL:B","4WI8:A","6DTD:C","6PZW:I","6MWV:F","7EG9:j","1W7O:A","7NNU:D","2QZI:A","3LUE:L","5UYU:A","5FHK:D","4KY4:H","4UUV:E","3LPJ:A","6TTQ:C","5OY7:g","2K1W:A","5QLU:A","4G4O:B","6EXN:o","3P6W:A","4R8P:A","4LF9:D","4LOX:B",
];

async function proteinChainToUniProtId(proteinChain: string) {
    const fetchResponse = await fetch('https://www.ebi.ac.uk/pdbe/api/mappings/uniprot/' + proteinChain);

    if (fetchResponse.ok) {
        const root = await fetchResponse.json();
        const firstLevelKeys = Object.keys(root);
        const thirdLevelKeys = Object.keys(root[firstLevelKeys[0]]['UniProt']);
    
        return thirdLevelKeys[0];
    }

    if (fetchResponse.status !== 404) {
        console.error(fetchResponse);
        throw new Error(fetchResponse.status.toString());
    }

    return null;
}

async function fetchMeta(object: ApiResult): Promise<PreparedRow> {
    const noChain = object.object_id.split(':');
    const uniProtId = await proteinChainToUniProtId(noChain[0]);

    if (uniProtId === null)
        return {
            name: object.object_id,
            distance: object.distance,
            organism: null,
            uniProtId: null,
        };

    const fetchResponse = await fetch('https://www.alphafold.ebi.ac.uk/api/prediction/' + uniProtId + '?key=AIzaSyCeurAJz7ZGjPQUtEaerUkBZ3TaBkXrY94');
    if (fetchResponse.ok) {
        const json = await fetchResponse.json();

        return {
            name: object.object_id,
            distance: object.distance,
            organism: json['0']['organismScientificName'],
            uniProtId: uniProtId,
        }
    }

    if (fetchResponse.status !== 404) {
        console.error(fetchResponse);
        throw new Error(fetchResponse.status.toString());
    }

    return {
        name: object.object_id,
        distance: object.distance,
        organism: null,
        uniProtId: uniProtId,
    };
}

export default function ProteinSearch() {
    const [inputValue, setInputValue] = useState('2VL3:B');
    const [searchResult, setSearchResult] = useState<SearchResult>({
        rows: [],
        meta: {
            searchTime: null,
            linearSearchTime: null,
            totalSearchTime: null,
        }
    });
    const [queryValue, setQueryValue] = useState('');
    const [isLoadingMore, setLoadingMore] = useState(false);    // Used for "Load More" button design/spinner
    const [loadingState, setLoadingState] = useState<LoadingState>(LoadingState.Initial);

    const fetchLimit = useRef(50);

    function fetchData(searchValue: string, loadingMore: boolean, onLoadFinishCallback: () => void) {
        const startTime = performance.now();

        setQueryValue(searchValue);
        setSearchResult(prev => ({
            rows: (loadingMore ? prev.rows : []),
            meta: {
                searchTime: null,
                linearSearchTime: null,
                totalSearchTime: null,
            },
        }));
        setLoadingState(loadingMore ? LoadingState.ShowingResults : LoadingState.Loading);

        fetch(`https://api.lmi-proteins.dyn.cloud.e-infra.cz/search?query=${searchValue}&similar=${fetchLimit.current}`)
        .then(response => {
            if (response.ok)
                return response.json();
        })
        .then((json: ApiResults) => {
            // Update known search times with results from API
            setSearchResult(prev => ({
                ...prev,
                meta: {
                    ...prev.meta,
                    searchTime: json.search_time,
                    linearSearchTime: json.linear_search_time,
                }
            }));

            const promises = json.results.map(result => {
                return fetchMeta(result);
            });

            Promise.all(promises)
            .then(responses => {
                setSearchResult(prev => ({
                    rows: responses,
                    meta: {
                        ...prev.meta,
                        // searchTime: json.search_time,
                        // linearSearchTime: json.linear_search_time,
                        totalSearchTime: (performance.now() - startTime) / 1000,
                    }
                }));
                onLoadFinishCallback();
                setLoadingState(LoadingState.ShowingResults);
            })
            .catch(error => {
                // Metadata request error
                console.error(error);

                setSearchResult({
                    rows: [],
                    meta: {
                        searchTime: 0,
                        linearSearchTime: 0,
                        totalSearchTime: 0,
                    },
                });
                onLoadFinishCallback();
                setLoadingState(LoadingState.ShowingResults);
            });
        })
        .catch(error => {
            // Query request error
            // TODO: finish when using final API
            // if (error.status !== 500) {
            //     console.error(error);
            //     throw new Error(error);
            // }
            console.log(error);

            setSearchResult({
                rows: [],
                meta: {
                    searchTime: 0,
                    linearSearchTime: 0,
                    totalSearchTime: 0,
                },
            });
            onLoadFinishCallback();
            setLoadingState(LoadingState.ShowingResults);
        });
    }

    function onSearch(event: FormEvent, value: string | undefined = undefined): void {
        event.preventDefault();

        if (value === undefined) {
            // Called as EVENT => new search query was typed
            if (queryValue === inputValue)
                // Prevent same search
                return;

            value = inputValue;
        } else
            // "Similar search" was performed
            setInputValue(value);

        // Reset limit if previously "load more" was used
        fetchLimit.current = 50;
        fetchData(value, false, () => {});
    }

    function loadMore(event: FormEvent): void {
        event.preventDefault();

        fetchLimit.current += 30;
        setLoadingMore(true);
        fetchData(inputValue, true, () => {
            setLoadingMore(false);
        });
    }

    return (
        <article className="container-fluid">
            <header className="visually-hidden">
                <h2 className="text-center">
                    LMI Protein Search
                </h2>
            </header>
            <section>
                <Row className="search-info">
                    <Col xs="12" xl="6" className="mb-4 mt-4 d-flex align-items-center">
                        <Form action="" method="POST" className={loadingState === LoadingState.Initial ? 'large': undefined} onSubmit={onSearch}>
                            <Form.Group>
                                <Form.Label>
                                    Type your protein chain
                                </Form.Label>
                                <InputGroup>
                                    <InputGroup.Text onClick={onSearch}><FontAwesomeIcon icon={faMagnifyingGlass} /></InputGroup.Text>
                                    <TextInput
                                        className={
                                            loadingState === LoadingState.ShowingResults && searchResult.rows.length === 0 ? 'form-control is-invalid' : 'form-control'
                                        }
                                        name="search"
                                        placeholder="Protein Chain"
                                        value={inputValue}
                                        onChange={(val: string) => setInputValue(val)}
                                        Component="input"
                                        maxOptions={6}
                                        options={inputHints}
                                        trigger={['']}
                                        spacer=""
                                    />
                                </InputGroup>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col xs="12" md="6" xl="3" className="mb-4 mb-sm-0">
                        <div className="ref-container">
                            {/* {
                                searchResult.rows.length > 0
                                ? ()
                                : undefined
                            } */}
                            <NglPreview uniProtIds={searchResult.rows.length !== 0 ? [searchResult.rows[0].uniProtId as string] : []} stageElementId='reference-3d' /* bgColor="#f8f9fa" */ />
                        </div>
                    </Col>
                    <Col xs="12" md="6" xl="3" className="query-info-container">
                        <LoadInfoBox visible={loadingState !== LoadingState.Initial} query={queryValue} data={{
                            searchTime: searchResult.meta.searchTime,
                            linearSearchTime: searchResult.meta.linearSearchTime,
                            totalSearchTime: searchResult.meta.totalSearchTime,
                        }} />
                    </Col>
                </Row>
                <Spinner style={{width: 100 + 'px', height: 100 + 'px', fontSize: 2.5 + 'rem'}} animation="border" variant="secondary" role="status" className={loadingState === LoadingState.Loading ? 'd-block mx-auto mt-5' : 'd-none'}>
                    <span className="visually-hidden">Loading...</span>
                </Spinner>
                <ProteinTable
                    queryValue={queryValue}
                    fetchResults={searchResult.rows}
                    visible={loadingState === LoadingState.ShowingResults && searchResult.rows.length !== 0}
                    isLoadingMore={isLoadingMore}
                    loadMore={loadMore}
                    onSearch={onSearch}
                />
                {loadingState === LoadingState.ShowingResults && searchResult.rows.length === 0 ? (
                    <div className="text-center disabled">
                        <i style={{fontSize: 1.35 + 'rem'}}>No results</i>
                    </div>
                ) : undefined}
            </section>
            {/* <footer></footer> */}
        </article>
    );
}
