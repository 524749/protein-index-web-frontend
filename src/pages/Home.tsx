import { Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../css/home.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCode, faDna, faImage } from "@fortawesome/free-solid-svg-icons";

export default function Home() {
    return (
        <article className="container home">
            <header>
                <h2>
                    The Learned Metric Index
                </h2>
                <Image src="imgs/logo.png" fluid className="app-logo"/>
            </header>
            <section>
                <p>
                    The <em>Learned Metric Index</em> (LMI) is a data structure for fast look-up of approximate nearest neighbor queries for high-dimensional (and metric) data.

                </p>
                <div className="quick-links">
                    <Link to="/protein-search"><FontAwesomeIcon icon={faDna} />Protein Demo</Link>
                    <Link to="/image-search"><FontAwesomeIcon icon={faImage} />Images Demo</Link>
                    <Link to="https://github.com/TerkaSlan/LMIF" target="blank"><FontAwesomeIcon icon={faCode} />GitHub</Link>
                </div>
                <h3>Publications</h3>
                <ol className="publications">
                    <li>
                        <Link to="https://scholar.google.com/citations?user=8FsPEREAAAAJ&hl=sk&oi=sra" target="blank">M. Antol</Link>, <Link to="https://scholar.google.com/citations?user=jxaLedQAAAAJ&hl=sk&oi=sra" target="blank">J. Oľha</Link>, <Link to="https://scholar.google.com/citations?user=aVhtRH0AAAAJ&hl=sk&oi=sra" target="blank">T. Slanináková</Link>, <Link to="https://scholar.google.com/citations?user=Vmj26i4AAAAJ&hl=sk&oi=sra" target="blank">V. Dohnal</Link><br />
                        Learned metric index—proposition of learned indexing for unstructured data.<br />
                        <em>Information Systems</em>100 (2021): 101774.
                        <div className="ref-links">
                            <Link to="https://doi.org/10.1016/j.is.2021.101774" target="blank">DOI</Link>
                        </div>
                    </li>
                    <li>
                        <Link to="https://scholar.google.com/citations?user=aVhtRH0AAAAJ&hl=sk&oi=sra" target="blank">T. Slanináková</Link>, <Link to="https://scholar.google.com/citations?user=8FsPEREAAAAJ&hl=sk&oi=sra" target="blank">M. Antol</Link>, <Link to="https://scholar.google.com/citations?user=jxaLedQAAAAJ&hl=sk&oi=sra" target="blank">J. Oľha</Link>, V. Kaňa, <Link to="https://scholar.google.com/citations?user=Vmj26i4AAAAJ&hl=sk&oi=sra" target="blank">V. Dohnal</Link><br />
                        Data-driven learned metric index: an unsupervised approach.<br />
                        <em>International Conference on Similarity Search and Applications</em>Springer, Cham, 2021. p. 81-94
                        <div className="ref-links">
                            <Link to="https://doi.org/10.1007/978-3-030-89657-7_7" target="blank">DOI</Link>
                            <Link to="https://www.sisap.org/2021/poster/14_poster.pdf" target="blank">Poster</Link>
                            <Link to="https://www.youtube.com/watch?v=LQf4IDWVODc" target="blank">Video</Link>
                        </div>
                    </li>
                    <li>
                        <Link to="https://scholar.google.com/citations?user=jxaLedQAAAAJ&hl=sk&oi=sra" target="blank">J. Oľha</Link>, <Link to="https://scholar.google.com/citations?user=aVhtRH0AAAAJ&hl=sk&oi=sra" target="blank">T. Slanináková</Link>, 
                        M. Gendiar, <Link to="https://scholar.google.com/citations?user=8FsPEREAAAAJ&hl=sk&oi=sra" target="blank">M. Antol</Link>, <Link to="https://scholar.google.com/citations?user=Vmj26i4AAAAJ&hl=sk&oi=sra" target="blank">V. Dohnal</Link><br />
                        Learned metric index—proposition of learned indexing for unstructured data.<br />
                        <em>Information Systems</em>100 (2021): 101774.
                        <div className="ref-links">
                            <Link to="https://doi.org/10.48550/arXiv.2208.08910" target="blank">DOI</Link>
                            <Link to="https://arxiv.org/pdf/2208.08910.pdf" target="blank">PDF</Link>
                        </div>
                    </li>
                </ol>
            </section>
            {/* <footer></footer> */}
        </article>
    );
}
