import { faFaceSadTear } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "../css/not-found.css";

export default function NotFound() {
    return (
        <article className="container">
            <header>
                <h2>
                    404 Not Found
                </h2>
            </header>
            <section>
                <p>
                    <FontAwesomeIcon icon={faFaceSadTear} className="icon-404" />
                </p>
            </section>
            {/* <footer></footer> */}
        </article>
    );
}
