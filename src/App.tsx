import MainFooter from "./components/MainFooter";
import MainNavbar from "./components/MainNavbar";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import ProteinSearch from "./pages/ProteinSearch";
import ImageSearch from "./pages/ImageSearch";
import NotFound from "./pages/NotFound";
import LoadingBar from "react-top-loading-bar";
import CustomRoutes from "./components/CustomSwitch";
import "./css/main.css";
import { useState } from "react";

export default function App() {
    const [progress, setProgress] = useState(0);

    return (
        <>
            <BrowserRouter>
                <LoadingBar color="#dc7ae8" height={3} progress={progress} onLoaderFinished={() => setProgress(0)} />
                {/* <header></header> */}
                {/* <aside></aside> */}
                <MainNavbar setProgress={setProgress} />
                <div className="d-flex flex-column main-container">
                    <main>
                        <CustomRoutes setProgress={setProgress}>
                            <Route path="/" element={<Home />}></Route>
                            <Route path="/about" element={<About />}></Route>
                            <Route path="/protein-search" element={<ProteinSearch />}></Route>
                            <Route path="/image-search" element={<ImageSearch />}></Route>
                            <Route path="*" element={<NotFound />}></Route>
                        </CustomRoutes>
                    </main>
                    <MainFooter />
                </div>
            </BrowserRouter>
        </>
    );
}
