import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Spinner } from "react-bootstrap";
import { onLoadMoreHeader } from "../pages/ProteinSearch";

export default function LoadMoreButton({ show, isLoading, onLoadMore }: {
    show: boolean;
    isLoading: boolean;
    onLoadMore: onLoadMoreHeader;
}) {
    if (!show)
        return (<></>);

    return (
        <Button className="load-more" onClick={isLoading ? undefined : onLoadMore} disabled={isLoading}>
            { isLoading ? (<Spinner as="span" animation="border" size="sm" role="status" aria-hidden="true"/>) : (
                <>
                    Load More
                    <FontAwesomeIcon icon={faArrowDown} />
                </>
            )
            }
        </Button>
    );
}
