import '../css/footer.css';

export default function MainFooter() {
    return (
        <>
            <footer className="main">
                <p>
                    Created at the DISA lab, 2022 | Contact: <a href="mailto:xslanin@mail.muni.cz">xslanin@mail.muni.cz</a>
                </p>
            </footer>
        </>
    );
}
