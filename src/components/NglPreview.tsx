import { faExpand } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Stage } from "ngl";
import { ChangeEvent, Dispatch, MutableRefObject, useEffect, useRef, useState } from "react"
import { Button, Form, OverlayTrigger, Spinner, Tooltip } from "react-bootstrap";
import "../css/ngl-preview.css";

export type ColorHEX = `#${string}`;

const colors: ColorHEX[] = [
    '#EE0000',
    '#0000FF',
];

enum PreviewState {
    Initial,
    Success,
    Error,
};

function nglViewer(stage: MutableRefObject<Stage | undefined>, uniProtIds: string[], setPreviewState: Dispatch<React.SetStateAction<PreviewState>>, stageElementId: string, bgColor: ColorHEX, representations: MutableRefObject<RepresentationParams>) {
    if (stage.current === undefined)
        stage.current = new Stage(stageElementId, {
            backgroundColor: bgColor,
        });

    const handler = function() {
        stage.current?.handleResize();
    };
    window.addEventListener('resize', handler, false);

    let promises = [];
    for (let i = 0; i < uniProtIds.length; i++) {
        promises.push(
            stage.current.loadFile('https://alphafold.ebi.ac.uk/files/AF-' + uniProtIds[i] + '-F1-model_v4.pdb',)
            .then(o => {
                o?.setName(i.toString());
                o?.addRepresentation('cartoon', {
                    color: colors[i],
                    // colorScale: 'Spectral',
                    // colorScheme: 'residueindex',
                    visible: true,
                });
                o?.addRepresentation('surface', {
                    sele: "polymer",
                    // colorScheme: "electrostatic",
                    color: colors[i],
                    // colorDomain: [-0.3, 0.3],
                    surfaceType: "av",
                    visible: false,
                });
                o?.addRepresentation('ball+stick', {
                    color: colors[i],
                    visible: false,
                });
                o?.addRepresentation('spacefill', {
                    color: colors[i],
                    visible: false,
                });
                o?.addRepresentation('trace', {
                    color: colors[i],
                    visible: false,
                });
                o?.addRepresentation('licorice', {
                    color: colors[i],
                    opacity: 0.7,
                    visible: false,
                });

                // o?.autoView();
                o?.eachRepresentation(r => {
                    const params = r.getParameters();
                    representations.current[r.name as keyof typeof representations.current] = {
                        colorData: params.colorData,
                        colorDomain: params.colorDomain,
                        colorMode:  params.colorMode,
                        colorReverse: params.colorReverse,
                        colorScale: params.colorScale,
                        colorScheme: params.colorScheme,
                        colorValue: params.colorValue,
                    };
                });

                return o;
            })
        );
    }

    Promise.all(promises)
    .then(responses => {
        if (responses.length > 1)
            // @ts-ignore
            (responses[0])?.superpose(responses[1]);

        (responses[0])?.updateRepresentations({
            position: true,
        });
        // responses[0].autoView();
        stage.current?.autoView();

        setTimeout(() => {
            setPreviewState(PreviewState.Success);
        }, 500);
    })
    .catch(error => {
        setPreviewState(PreviewState.Error);
        console.error(error);
    });

    return handler;
}

function handleRepresentationChange(event: ChangeEvent<HTMLSelectElement>, stage?: Stage) {
    event.preventDefault();
    const val = event.currentTarget.value;

    stage?.eachComponent(c => {
        c.eachRepresentation(r => {
            if (r.name === val) {
                r.setVisibility(true);
                return;
            }

            r.setVisibility(false);
        });
    });
}

function makeControls(show: boolean, parts: string[], stage: Stage | undefined, isPartVisible: boolean[], setIsPartVisible: Dispatch<React.SetStateAction<boolean[]>>) {
    if (!show)
        return (<></>);
    
    return (
        <div className="controls">
            <OverlayTrigger overlay={
                <Tooltip>
                    Toggle Fullscreen
                </Tooltip>
            }>
                <Button className="fullscreen-btn" onClick={
                    () => {
                        if (stage !== null)
                            // @ts-ignore
                            stage.toggleFullscreen();
                    }
                }>
                    <FontAwesomeIcon icon={faExpand} />
                </Button>
            </OverlayTrigger>
            <div>
                <Form.Group>
                    {/* <Form.Label>Representation</Form.Label> */}
                    <Form.Select aria-label="Select representation" size="sm" onChange={event => handleRepresentationChange(event, stage)} defaultValue='cartoon'>
                        <option disabled>Select representation</option>
                        <option value='cartoon'>Cartoon</option>
                        <option value='surface'>Surface</option>
                        <option value='ball+stick'>Ball + Stick</option>
                        <option value='spacefill'>Spacefill</option>
                        <option value='trace'>Trace</option>
                        <option value='licorice'>Licorice</option>
                    </Form.Select>
                </Form.Group>
            </div>
            <div>
                <Form.Group>
                    <Form.Check type="switch" id={parts[0]} label={(
                        <OverlayTrigger overlay={
                            <Tooltip>
                                {parts[0]}
                            </Tooltip>
                        }>
                            <div className="text-truncate" style={{maxWidth: 70 + "px", color: '#ff0000'}}>{parts[0]}</div>
                        </OverlayTrigger>
                    )} checked={isPartVisible[0]}  onChange={_ => {
                        // hidePart(stage, parts[0], !isPartVisible[0]);
                        setIsPartVisible(prev => [!prev[0], prev[1]]);
                    }} />
                </Form.Group>
                <Form.Group>
                    <Form.Check type="switch" id={parts[1]} label={(
                        <OverlayTrigger overlay={
                            <Tooltip>
                                {parts[1]}
                            </Tooltip>
                        }>
                            <div className="text-truncate" style={{maxWidth: 70 + "px", color: '#0000ff'}}>{parts[1]}</div>
                        </OverlayTrigger>
                    )} checked={isPartVisible[1]} onChange={_ => {
                        // hidePart(stage, parts[1], !isPartVisible[1]);
                        setIsPartVisible(prev => [prev[0], !prev[1]]);
                    }} />
                </Form.Group>
            </div>
        </div>
    );
}

function changeObjectVisibility(stage: Stage, name: string, visible: boolean) {
    stage.eachComponent(c => {
        if (c.name === name)
            c.setVisibility(visible);
    });
}

function manageVisibleObjects(stage: Stage, isPartVisible: boolean[], representations: RepresentationParams) {
    let visibleCount = 0;
    
    for (let i = 0; i < isPartVisible.length; i++) {
        changeObjectVisibility(stage, i.toString(), isPartVisible[i]);

        if (isPartVisible[i])
            visibleCount++;
    }

    // for (let i = 0; i < stage.compList.length; i++) {
    //     const c = stage.compList[i];

    //     c.eachRepresentation(r => {
    //         const currentParam = r.getParameters();

    //         if (visibleCount < 2) {
    //             r.setParameters({
    //                 ...currentParam,
    //                 ...representations[r.name as keyof RepresentationParams],
    //             });

    //             return;
    //         }

    //         r.setParameters({
    //             ...currentParam,
    //             ...representations[r.name as keyof RepresentationParams],
    //             color: colors[i],
    //         });
    //     });
    // }
}

function cleanUpStage(stage: MutableRefObject<Stage | undefined>) {
    if (stage.current === undefined)
        return;

    stage.current.dispose();
    stage.current.removeAllComponents();

    stage.current = undefined;
}

type RepresentationParams = {
    'cartoon': {},
    'surface': {},
    'ball+stick': {},
    'spacefill': {},
    'trace': {},
    'licorice': {},
};

type Props = {
    uniProtIds: string[];
    stageElementId: string;
    bgColor?: ColorHEX;
    allowFullscreen?: boolean;
};

export default function NglPreview({ uniProtIds, stageElementId, bgColor = '#ffffff', allowFullscreen = false }: Props ) {
    const [previewState, setPreviewState] = useState(PreviewState.Initial);
    const [isPartVisible, setIsPartVisible] = useState([true, true]);
    const stage = useRef<Stage | undefined>(undefined);
    const representations = useRef<RepresentationParams>({
        'cartoon': {},
        'surface': {},
        'ball+stick': {},
        'spacefill': {},
        'trace': {},
        'licorice': {},
    });

    useEffect(() => {
        if (uniProtIds.length === 0)
            return;

        setPreviewState(PreviewState.Initial);
        const handler = nglViewer(stage, uniProtIds, setPreviewState, stageElementId, bgColor, representations);

        // Deregister handler on dismount
        return () => window.removeEventListener('click', handler);
    }, [JSON.stringify(uniProtIds)]);

    useEffect(() => {
        if (stage.current === undefined)
            return;
        
        manageVisibleObjects(stage.current, isPartVisible, representations.current);
    }, [isPartVisible]);

    if (uniProtIds.length === 0)
        cleanUpStage(stage);
    
    return (
        <div className="ngl-container">
            {uniProtIds.length !== 0 ? (
                <>
                    {makeControls(allowFullscreen, uniProtIds, stage.current, isPartVisible, setIsPartVisible)}
                    <Spinner animation="border" role="status" variant="secondary" className={previewState !== PreviewState.Initial ? 'd-none mx-auto' : 'mx-auto' } >
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                    <div id={stageElementId} className={previewState === PreviewState.Success ? 'w-100 h-100' : 'w-100 h-100 visually-hidden'}></div>
                    {previewState === PreviewState.Error ? (<span className="disabled"><i>Error while loading 3D preview</i></span>) : undefined}
                </>
            ) : undefined}
        </div>
    );
}
