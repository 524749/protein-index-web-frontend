import { Spinner } from "react-bootstrap";
import "../css/load-info-box.css";

type Props = {
    visible: boolean;
    query: string;
    data: {
        searchTime: number | null;
        linearSearchTime: number | null;
        totalSearchTime: number | null;
    }
};

const elements = [
    {
        placeholder: 'Search Time',
        key: 'searchTime',
    },
    {
        placeholder: 'Linear Search Time',
        key: 'linearSearchTime',
    },
    {
        placeholder: 'Total Search Time',
        key: 'totalSearchTime',
    }
];

export default function LoadInfoBox({ visible, query, data }: Props) {
    return (
        <ul className={visible ? 'load-info' : 'load-info-box d-none'}>
            <li>
                Query:<b className="ms-2">{query}</b>
            </li>

            {elements.map(e => {
                const val = data[e.key as keyof typeof data];

                return (
                    <li key={e.key}>
                        {e.placeholder}:
                        {val === null ? (
                            <Spinner animation="border" variant="secondary" role="status" size="sm" className="ms-2">
                                <span className="visually-hidden">Loading...</span>
                            </Spinner>
                        ) : (
                            <b className="ms-2">{val.toFixed(3)} s</b>
                        )}
                    </li>
                );
            })}
        </ul>
    );
}
