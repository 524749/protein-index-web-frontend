import { Button, Modal, OverlayTrigger, Popover, Table } from "react-bootstrap";
import NglPreview from "./NglPreview";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComputerMouse, faGamepad, faKeyboard } from "@fortawesome/free-solid-svg-icons";
import "../css/comparison-3d-modal.css";

const controlHintPopover = (
    <Popover id="popover-basic" style={{
        // fontSize: 1.1 + 'rem',
        width: 'auto',
        maxWidth: 450 + 'px',
    }}>
        {/* <Popover.Header as="h3">Controls</Popover.Header> */}
        <Popover.Body>
            <Table borderless striped>
                <tbody>
                    <tr>
                        <td><FontAwesomeIcon icon={faComputerMouse} /> Left Click Hold + Move</td>
                        <td>Rotate</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faComputerMouse} /> Right Click Hold + Move</td>
                        <td>Up/Down/Left/Right</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faKeyboard} /> Shift + <FontAwesomeIcon icon={faComputerMouse} /> Left Click + Move</td>
                        <td>Front/Rear</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faKeyboard} /> Shift + <FontAwesomeIcon icon={faComputerMouse} /> Wheel</td>
                        <td>Hide distant parts</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faKeyboard} /> R</td>
                        <td>Reset position in 3D space</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faKeyboard} /> ESC</td>
                        <td>Exit fullscreen mode</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faComputerMouse} /> Hover on Protein</td>
                        <td>Atom details</td>
                    </tr>
                    <tr>
                        <td><FontAwesomeIcon icon={faComputerMouse} /> Double Left Click</td>
                        <td>Atom as center of rotation</td>
                    </tr>
                </tbody>
            </Table>
        </Popover.Body>
    </Popover>
);

type Props = {
    show: boolean;
    onHide: () => void;
    referenceUniProtId: string | null;
    comparingUniProtId: string | null;
};

export default function Comparison3DModal({ show, onHide, referenceUniProtId, comparingUniProtId }: Props) {
    const areNotNull = referenceUniProtId !== null && comparingUniProtId !== null;
    let uniProtIds: string[] = [];
    if (areNotNull) {
        uniProtIds.push(referenceUniProtId);
        uniProtIds.push(comparingUniProtId);
    }

    return (
        <Modal
            show={show && areNotNull}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            fullscreen="xl-down"
        >
            <Modal.Header closeButton className="p-1 pe-3">
                <Modal.Title id="contained-modal-title-vcenter" className="w-100">
                    {/* <span style={{color: '#EE0000'}}>{referenceUniProtId} (query)</span> <span className="disabled small">/</span> <span style={{color: '#0000ff'}}>{comparingUniProtId}</span> */}
                    <OverlayTrigger trigger="hover" placement="bottom" overlay={controlHintPopover}>
                        <Button variant="link" className="controls-info-btn"><FontAwesomeIcon icon={faGamepad} className="control-hint-btn"/>How to move in 3D space?</Button>
                    </OverlayTrigger>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="p-0 pb-2">
                <div className="comparison-3d">
                    <NglPreview uniProtIds={uniProtIds} stageElementId='protein-overlapping' allowFullscreen={true} />
                </div>
            </Modal.Body>
        </Modal>
    );
}
