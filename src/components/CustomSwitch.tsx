import { useEffect, useState } from "react";
import { Routes, useLocation } from "react-router-dom";

type Props = {
    setProgress: (value: number) => void;
    children: JSX.Element | JSX.Element[];
};

export default function CustomRoutes({ setProgress, children } : Props) {
    const location = useLocation();
    const [prevLoc, setPrevLoc] = useState('');

    useEffect(() => {
        setPrevLoc(location.pathname);
        setProgress(100);
        setInterval(() => setProgress(0), 1000);

        if (location.pathname === prevLoc)
            setPrevLoc('');
    }, [location]);

    return (
        <Routes>
            {children}
        </Routes>
    );
}
