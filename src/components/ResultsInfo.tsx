import { Col } from "react-bootstrap";

type Props = {
    queryValue: string;
    filteredCount: number;
    totalCount: number
};

export default function ResultsInfo({ queryValue, filteredCount, totalCount }: Props) {
    return (
        <Col className="disabled">
            Most similar proteins to <i>{queryValue}</i> (showing {filteredCount} of {totalCount} total results)
        </Col>
    );
}
