import { Button, Col, Form, OverlayTrigger, Row, Table, Tooltip } from "react-bootstrap";
import { createColumnHelper, flexRender, getCoreRowModel, getFilteredRowModel, getSortedRowModel, useReactTable, getGroupedRowModel, getExpandedRowModel, Row as TableRow, ColumnFiltersState, SortingState, Column, CellContext, HeaderGroup } from "@tanstack/react-table";
import { FormEvent, useEffect, useState } from "react";
import { faArrowDown, faArrowUp, faCube, faHashtag, faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ProteinTableRow from "./ProteinTableRow";
import { Link } from "react-router-dom";
import Comparison3DModal from "./Comparison3DModal";
import ResultsInfo from "./ResultsInfo";
import LoadMoreButton from "./LoadMoreButton";
import { onLoadMoreHeader, onSearchHeader, PreparedRow } from "../pages/ProteinSearch";

export type Record = {
    name: string;
    uniProtId: string | null;
    rmsd: string;
    organism: string | null;
    alpha: string;
    beta: string;
    gamma: string;
    bar: number;
};

export interface CustomCellContext extends CellContext<Record, unknown> {
    meta: {
        isExpanded: boolean;
        subRowsCount: number;
        referenceUniProtId: string | null;
        onSearch: onSearchHeader;
        onClickModal: (event: FormEvent) => void;
    };
};

function randomFloat(): number {
    return Math.random();
}

function getBestRow(rows: TableRow<Record>[]): TableRow<Record> {
    let minIndex = 0,
        minValue = Number.MAX_SAFE_INTEGER;
    
    for(let index = 0; index < rows.length; index++) {
        const rmsd: number = rows[index].getValue('rmsd');

        if (rmsd < minValue) {
            minValue = rmsd;
            minIndex = index;
        }
    }

    return rows[minIndex];
}

const columnHelper = createColumnHelper<Record>();
const columns = [
    columnHelper.accessor('name', {
        header: () => <>UniProt ID</>,
        cell: context => (
            context.row.original.uniProtId === null
            ? (<i>Unknown</i>)
            : (<Link to={ 'https://alphafold.ebi.ac.uk/entry/' + context.row.original.uniProtId} target="blank">{context.row.original.uniProtId}</Link>)
        ),
        aggregatedCell: context => (
            context.row.original.uniProtId === null
            ? (<i>Unknown</i>)
            : (<Link to={ 'https://alphafold.ebi.ac.uk/entry/' + context.row.original.uniProtId} target="blank">{context.row.original.uniProtId}</Link>)
        ),
        aggregationFn: (_, childRows) => getBestRow(childRows).getValue('name'),
        sortingFn: (rowA, rowB) => {
            if (rowA.original.uniProtId === null || rowB.original.uniProtId === null)
                return 0;

            if (rowA.original.uniProtId === rowB.original.uniProtId)
                return 0;

            if (rowA.original.uniProtId < rowB.original.uniProtId)
                return -1;

            return 1;
        },
    }),
    columnHelper.accessor('rmsd', {
        header: () => <>RMSD</>,
        cell: context => context.renderValue(),
        aggregationFn: 'min',
    }),
    columnHelper.accessor('organism', {
        header: () => <>Organism</>,
        cell: context => (
            context.row.original.organism === null
            ? (<i>Unknown</i>)
            : (
                <OverlayTrigger overlay={
                    <Tooltip>
                        {context.renderValue()}
                    </Tooltip>
                }>
                    <div className="text-truncate" style={{maxWidth: 300 + "px"}}>{context.renderValue()}</div>
                </OverlayTrigger>
            )
        ),
        aggregatedCell: context => (
            context.row.original.organism === null
            ? (<i>Unknown</i>)
            : (
                <OverlayTrigger overlay={
                    <Tooltip>
                        {context.renderValue()}
                    </Tooltip>
                }>
                    <div className="text-truncate" style={{maxWidth: 300 + "px"}}>{context.renderValue()}</div>
                </OverlayTrigger>
            )
        ),
        enableColumnFilter: true,
    }),
    columnHelper.accessor('alpha', {
        header: () => <>Alpha</>,
        cell: context => context.renderValue(),
        aggregationFn: (_, childRows) => getBestRow(childRows).getValue('alpha'),
    }),
    columnHelper.accessor('beta', {
        header: () => <>Beta</>,
        cell: context => context.renderValue(),
        aggregationFn: (_, childRows) => getBestRow(childRows).getValue('beta'),
    }),
    columnHelper.accessor('gamma', {
        header: () => <>Gamma</>,
        cell: context => context.renderValue(),
        aggregationFn: (_, childRows) => getBestRow(childRows).getValue('gamma'),
    }),
    columnHelper.accessor('bar', {
        header: () => <>Position in Query</>,
        cell: context => (
            <div className="progress" role="progressbar" aria-label="Position in Query" aria-valuenow={context.getValue()} aria-valuemin={0} aria-valuemax={100} style={{
                minWidth: 300 + "px",
                backgroundColor: "lightgray",
            }}>
                <div className="progress-bar" style={{
                    width: context.getValue() + "%",
                }}></div>
            </div>
        ),
        aggregatedCell: context => (
            <div className="progress" role="progressbar" aria-label="Position in Query" aria-valuenow={context.getValue()} aria-valuemin={0} aria-valuemax={100} style={{
                minWidth: 300 + "px",
                backgroundColor: "lightgray",
            }}>
                <div className="progress-bar" style={{
                    width: context.getValue() + "%",
                }}></div>
            </div>
        ),
        aggregationFn: (_, childRows) => getBestRow(childRows).getValue('bar'),
        sortingFn: (rowA, rowB) => {
            if (rowA.original.rmsd === rowB.original.rmsd)
                return 0;

            if (rowA.original.rmsd < rowB.original.rmsd)
                return -1;
            
            return 1;
        },
    }),
    columnHelper.display({
        header: () => <FontAwesomeIcon icon={faHashtag} />,
        id: "actions",
        cell: context => {
            const contextCasted = context as CustomCellContext;

            return (
                <div style={{minWidth: 80 + 'px'}}>
                    {contextCasted.meta.referenceUniProtId !== null && contextCasted.row.original.uniProtId !== null ? (
                        <Button className="show-3d p-0 mx-2" onClick={contextCasted.meta.onClickModal} variant="link"><FontAwesomeIcon icon={faCube} /></Button>
                    ) : undefined}

                    <Button className="p-0 mx-2" onClick={
                        event => contextCasted.meta.onSearch(event, contextCasted.row.original.name)
                    } variant="link"><FontAwesomeIcon icon={faMagnifyingGlass} /></Button>
                </div>
            );
        },
    }),
];

function prepareRow(object: PreparedRow): Record {
    const barValue = object.distance <= 1 ? Math.round((1 - object.distance) * 100) : 0;

    return {
        name: object.name,
        uniProtId: object.uniProtId,
        rmsd: object.distance.toFixed(7),
        organism: object.organism,
        alpha: randomFloat().toFixed(4),
        beta: randomFloat().toFixed(4),
        gamma: randomFloat().toFixed(4),
        bar: barValue,
    };
}

function handleFilter(value: string, column: Column<Record, unknown>): void {
    column.setFilterValue(value);
}

const sortDirectionIcons = {
    'asc': <FontAwesomeIcon icon={faArrowUp} />,
    'desc': <FontAwesomeIcon icon={faArrowDown} />,
};

function makeTableHead(headerGroups: HeaderGroup<Record>[]) {
    return (
        <thead>
        {headerGroups.map(headerGroup => 
            <tr key={headerGroup.id}>
            {headerGroup.headers.map(header => 
                <th key={header.id} className={'user-select-none ' + header.id}>
                    {header.isPlaceholder ? undefined : (
                        <>
                            <div
                                className={header.column.getCanSort() ? 'cursor-pointer' : ''}
                                onClick={header.column.getToggleSortingHandler()}
                            >
                                {flexRender(header.column.columnDef.header, header.getContext())}

                                <div className="sort-icon">
                                    {sortDirectionIcons[header.column.getIsSorted() as keyof typeof sortDirectionIcons] ?? undefined}
                                </div>
                            </div>
                            {
                                header.column.id === 'organism'
                                ? (<div><Form.Control className="filter" name="filter" placeholder="Filter" onChange={event => handleFilter(event.target.value, header.column)} /></div>)
                                : undefined
                            }
                        </>
                    )}
                </th>
            )}
            </tr>
        )}
        </thead>
    )
}

type Props = {
    queryValue: string;
    fetchResults: PreparedRow[];
    loadMore: onLoadMoreHeader;
    visible: boolean;
    isLoadingMore: boolean;
    onSearch: onSearchHeader;
};

export default function ProteinTable({ queryValue, fetchResults, loadMore, visible, isLoadingMore, onSearch }: Props) {
    const [proteinComparisonModalValue, setProteinComparisonModalValue] = useState<string | null>(null);
    const [tableData, setTableData] = useState<Record[]>([]);
    // React Table states
    const [sorting, setSorting] = useState<SortingState>([{
        id: 'rmsd',
        desc: false,
    }]);
    const [columnFilters, setColumnFilters] = useState<ColumnFiltersState>([]);
    const [grouping, setGrouping] = useState([
        'organism',
    ]);

    useEffect(() => {
        if (fetchResults.length === 0) {
            setTableData([]);

            return;
        }

        let tableDataPrepared: Record[] = [];
        fetchResults.forEach(fetchResult => {
            tableDataPrepared.push(prepareRow(fetchResult));
        });

        setTableData(tableDataPrepared);
    }, [JSON.stringify(fetchResults)]);

    const table = useReactTable({
        data: tableData,
        columns,
        state: {
            grouping,
            sorting,
            columnFilters,
        },
        getCoreRowModel: getCoreRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        getGroupedRowModel: getGroupedRowModel(),
        getExpandedRowModel: getExpandedRowModel(),
        onSortingChange: setSorting,
        onColumnFiltersChange: setColumnFilters,
        onGroupingChange: setGrouping,
        autoResetExpanded: false,
        // meta: {
        //     updateData: (rowIndex, columnId, newValue) => {
        //         setTableData(old => old.map((row, index) => {
        //             if (index === rowIndex)
        //                 return {
        //                     ...old[rowIndex],
        //                     [columnId]: newValue,
        //                 };

        //             return row;
        //         }));
        //     },
        // },
    });

    return (
        <div className={visible ? undefined : 'visually-hidden'}>
            <Comparison3DModal
                show={proteinComparisonModalValue !== null}
                onHide={() => setProteinComparisonModalValue(null)}
                referenceUniProtId={table.getRowModel().rows.length > 0 ? table.getRowModel().rows[0].original.uniProtId : null}
                comparingUniProtId={proteinComparisonModalValue}
            />
            <Row className="table-pre">
                <ResultsInfo queryValue={queryValue} filteredCount={table.getPreGroupedRowModel().rows.length} totalCount={fetchResults.length} />
            </Row>
            <Table responsive hover borderless className="protein-table">
                {makeTableHead(table.getHeaderGroups())}
                <tbody>
                    {table.getRowModel().rows.map(row => 
                        <ProteinTableRow
                            key={row.id}
                            row={row}
                            setProteinComparisonModalValue={setProteinComparisonModalValue}
                            onSearch={onSearch}
                            referenceUniProtId={table.getRowModel().rows.length > 0 ? table.getRowModel().rows[0].original.uniProtId : null}
                        />
                    )}
                </tbody>
            </Table>
            <Row className="table-post">
                <ResultsInfo queryValue={queryValue} filteredCount={table.getPreGroupedRowModel().rows.length} totalCount={fetchResults.length} />
                <Col md="4">
                    <LoadMoreButton
                        show={
                            columnFilters.length === 0
                            // && sorting.filter(element => element.id === 'rmsd').length > 0
                            && table.getPrePaginationRowModel().rows.length > 0
                        }
                        onLoadMore={loadMore}
                        isLoading={isLoadingMore} />
                </Col>
                <Col md="4"></Col>
            </Row>
        </div>
    );
}
