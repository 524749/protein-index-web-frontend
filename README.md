# Protein Index Web Application
## Reference
https://github.com/TerkaSlan/LMIF

## Instructions to run the application
1. git clone https://gitlab.ics.muni.cz/524749/protein-index-web-frontend.git
2. cd protein-index-web-frontend
3. npm install
4. npm run
5. (Enjoy)

### Or via Docker
1. git clone https://gitlab.ics.muni.cz/524749/protein-index-web-frontend.git
2. docker build -t protein-index-web-app .

## Automatic deployment
The deployment follows the development cycle:

### Production

Master branch creates deployment at https://proteins.dyn.cloud.e-infra.cz after manual run of "production" stage of CI/CD pipeline, this should be triggered after testing the the app on `staging` environment

### Staging

Each commit to master (merge request) creates update at stagging environment located at https://staging.proteins.dyn.cloud.e-infra.cz/

### Revision

Used for testing currently developed features.   
Feature branch creates a target at `https://<normalized_branch_name>.rev.proteins.dyn.cloud.e-infra.cz`

TLS might not apply CNs of certificates are limited to only 63bytes.

## Meta
- Author: Jakub Čillík
- Email: 524749@mail.muni.cz

## Contribution

## License